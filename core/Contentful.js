import markdown from './Markdown';

import { contentfulClient } from './ContentfulConfig';

export const getCategories = function(Store) {
	return contentfulClient.getEntries({
		content_type: 'category'
	})
	.then(response => {
		for (let i = 0; i < response.items.length; i++) {
			const category = response.items[i].fields;
			Store.dispatch('addCategory', category);
		}

		return new Promise(resolve => { resolve(Store.getters.categories); });
	});
};

export const getTags = function(Store) {
	return contentfulClient.getEntries({
		content_type: 'tags'
	})
	.then(response => {
		for (let i = 0; i < response.items.length; i++) {
			const tag = response.items[i].fields;
			Store.dispatch('addTag', tag);
		}

		return new Promise(resolve => { resolve(Store.getters.tags); });
	});
};

export const getLinks = function(Store) {
	return contentfulClient.getEntries({
		content_type: 'menu'
	})
	.then(response => {
		response.items.forEach(link => {
			Store.dispatch('addLink', link);
		});
		return new Promise( resolve => {resolve(Store.getters.links);});
	});
};

export const getBlocks = function(Store) {
	return contentfulClient.getEntries({
		content_type: 'block'
	})
	.then(response => {
		for (let i = 0; i < response.items.length; i++) {
			if(response.items[i].fields.title == 'About') {
				let block = response.items[i].fields;
				if(typeof block.body == 'string') {
					block.body = markdown.render(block.body);
				}
				Store.dispatch('setAbout', response.items[i].fields);
			}
		}

		return new Promise(resolve => { resolve(Store.getters.about); });
	});
};

export const getPosts = function(Store) {
	return contentfulClient.getEntries({
		content_type: 'post',
		order: '-sys.createdAt'
	})
	.then(response => {
		for (let i = 0; i < response.items.length; i++) {
			const item = response.items[i];
			let post = item.fields;
			post.path = '/' + post.slug;
			post.date = item.sys.createdAt;
			if(typeof post.body == 'string') {
				post.body = markdown.render(post.body);
			}

			if(typeof post.footerLink == 'string') {
				post.footerLink = markdown.renderInline(post.footerLink);
			}

			Store.dispatch('addPost', post);
		}
		Store.dispatch('setFeaturedPosts');
		return new Promise(resolve => { resolve(Store.getters.posts); });
	});
};

export const getProducts = function(Store) {
	return contentfulClient.getEntries({
		content_type: 'products',
		order: 'fields.order'
	})
	.then(response => {
		for (let i = 0; i < response.items.length; i++) {
			const product = response.items[i].fields;
			Store.dispatch('addProduct', product);
		}
		return new Promise(resolve => { resolve(Store.getters.products); });
	});
};

export const getAll = function(store) {
	let promises = [
		getCategories(store),
		getTags(store),
		getPosts(store),
		getBlocks(store),
		getProducts(store),
		getLinks(store)
	];

	return Promise.all(promises).then(() => {
		store.dispatch('hasLoaded');
	});
};
