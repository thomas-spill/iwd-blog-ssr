import Vuex from 'vuex';

const store = () => new Vuex.Store({
	state: {
		categories: [],
		tags: [],
		products: [],
		currentCategory: {},
		currentTag: {},
		posts: [],
		featuredPosts: [],
		filteredPosts: [],
		currentPosts: [],
		relatedPosts: [],
		currentPost: {},
		currentFilter: {
			type: false,
			name: false
		},
		about: {},
		links: [],

		uiData: {
			viewportSize: {
				width: 0,
				height: 0
			}
		},

		href: '',

		loaded: false
	},

	mutations: {
		ADD_CATEGORY(state, category) {
			state.categories.push(category);
		},
		ADD_LINK(state, link) {
			state.links.push(link);
		},
		ADD_TAG(state, tag) {
			state.tags.push(tag);
		},
		ADD_PRODUCT(state, product) {
			state.products.push(product);
		},
		SET_CURRENT_CATEGORY(state, name) {
			state.currentCategory = state.categories.find(o => o.name == name);
		},
		SET_CURRENT_TAG(state, name) {
			state.currentTag = state.tag.find(o => o.name == name);
		},
		ADD_POST(state, post) {
			state.posts.push(post);
		},
		SET_CURRENT_POST(state, path) {
			state.currentPost = path == '/' ? {} : state.posts.find(o => o.path == path);
			state.relatedPosts = state.posts.filter(p => p.slug != state.currentPost.slug).filter(o => o.category.fields.name == state.currentPost.category.fields.name).slice(0, 4);
		},
		SET_CURRENT_FILTER(state, filter) {
			state.currentPosts = [];
			state.currentFilter = filter;
			state.filteredPosts = state.posts.filter(p => {
				if(p[filter.type]) {
					if(Array.isArray(p[filter.type])) {
						return p[filter.type].find(e => e.fields.name.toLowerCase() == filter.name.toLowerCase());
					} else {
						return p[filter.type].fields.name.toLowerCase() == filter.name.toLowerCase();
					}
				}
				return false;
			});
		},
		SET_FEATURED_POSTS(state) {
			state.featuredPosts = state.posts.filter(p => p.featured);
		},
		LOAD_MORE_CURRENT_POSTS(state) {
			if(state.currentFilter.type == false || state.currentFilter.type == undefined) {
				state.filteredPosts = state.posts;
			}

			if(state.currentPosts.length < state.filteredPosts.length) {
				const begin = state.currentPosts.length - 1 > -1 ? state.currentPosts.length - 1 : 0;
				const end = state.currentPosts.length + 9;

				state.currentPosts = state.currentPosts.concat(state.filteredPosts.slice(begin, end));
			}
		},
		RESET_CURRENT_POSTS(state) {
			state.currentPosts = [];
		},
		RESET_CURRENT_FILTER(state) {
			state.currentFilter = { type: false, name: false };
		},
		SET_ABOUT(state, block) {
			state.about = block;
		},
		UPDATE_VIEWPORT_SIZE(state, size) {
			state.uiData.viewportSize = size;
		},
		SET_HREF(state, href) {
			state.href = href;
		},
		HAS_LOADED(state) {
			state.loaded = true;
		}
	},

	actions: {
		addCategory({ commit }, category) {
			commit('ADD_CATEGORY', category);
		},

		addLink({ commit }, link) {
			commit('ADD_LINK', link);
		},

		addTag({ commit }, tag) {
			commit('ADD_TAG', tag);
		},

		addProduct({ commit }, product) {
			commit('ADD_PRODUCT', product);
		},

		setCurrentCategory({ commit }, name) {
			commit('SET_CURRENT_CATEGORY', name);
		},

		setCurrentTag({ commit }, name) {
			commit('SET_CURRENT_TAG', name);
		},

		addPost({ commit }, post) {
			commit('ADD_POST', post);
		},

		setCurrentPost({ commit }, path) {
			commit('SET_CURRENT_POST', path);
		},

		setCurrentFilter({ commit }, path) {
			commit('SET_CURRENT_FILTER', path);
		},

		resetCurrentFilter({ commit }, path) {
			commit('RESET_CURRENT_FILTER', path);
		},

		setFeaturedPosts({ commit }) {
			commit('SET_FEATURED_POSTS');
		},

		loadMoreCurrentPosts({ commit }) {
			commit('LOAD_MORE_CURRENT_POSTS');
		},

		resetCurrentPosts({ commit }) {
			commit('RESET_CURRENT_POSTS');
		},

		setAbout({ commit }, block) {
			commit('SET_ABOUT', block);
		},

		updateViewportSize({ commit }, size) {
			commit('UPDATE_VIEWPORT_SIZE', size);
		},

		setHref({ commit }, href) {
			commit('SET_HREF', href);
		},

		hasLoaded({ commit }) {
			commit('HAS_LOADED');
		}
	},

	getters: {
		categories: state => state.categories,
		links: state => state.links,
		tags: state => state.tags,
		products: state => state.products,
		currentCategory: state => state.currentCategory,
		currentTag: state => state.currentTag,
		posts: state => state.posts,
		filteredPosts: state => state.filteredPosts,
		featuredPosts: state => state.featuredPosts,
		relatedPosts: state => state.relatedPosts,
		currentPosts: state => state.currentPosts,
		currentPost: state => state.currentPost,
		currentFilter: state => state.currentFilter,
		about: state => state.about,

		viewportSize: state => state.uiData.viewportSize,

		href: state => state.href,

		loaded: state => state.loaded
	}
});

export default store;
