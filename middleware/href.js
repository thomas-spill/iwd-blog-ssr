export default function(context) {
	const href = context.isServer ? '//' + context.req.headers['host'] + context.req.originalUrl : window.location.href;

	context.store.dispatch('setHref', href);
};
