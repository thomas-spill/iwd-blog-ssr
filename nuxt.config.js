module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'I Wonder & Dream - IWD Magazine',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'I Wonder & Dream - IWD Magazine' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#3B8070' },

  router: {
    middleware: ['href'],
    extendRoutes (routes, resolve) {
      routes.push({
        name: 'categories',
        path: '/categories/:category',
        component: resolve(__dirname, 'pages/index.vue')
      })
    },
    scrollBehavior(to, from, savedPosition) {
      if(to.name == 'categories' || from.name == "index") {
        return {x: document.documentElement.scrollLeft, y: document.documentElement.scrollTop }; 
      } else {
        return {x: 0, y: 0};
      }
    }
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLINT on save
    */
    extend (config, ctx) {
      if (ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    },
    preLoaders: [
    ],
    loaders: [
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        loaders: ['style', 'css', 'sass']
      },
      {
        test: /\.svg$/,
        loader: 'svg-inline-loader'
      }
    ]
  }
}
